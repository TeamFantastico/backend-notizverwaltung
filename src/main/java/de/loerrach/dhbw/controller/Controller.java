package de.loerrach.dhbw.controller;

// Interne Verweise
import de.loerrach.dhbw.entity.Notiz;
import de.loerrach.dhbw.service.NotizenService;

// Externe Verweise
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Logger;

@Path("/api")
@Produces(MediaType.APPLICATION_JSON)

public class Controller {

    // Console Logger
    private static final Logger LOGGER = Logger.getLogger(Controller.class.getName());

    // Service
    @Inject
    private NotizenService notizenService;

    // Hello World Text
    @GET
    public String getHelloWorld()
    {
        return "Hello World";
    }

    // Notiz hinzufügen
    @POST
    @Path("/notiz")
    @Consumes(MediaType.APPLICATION_JSON)
    public Notiz postNotice(@Valid Notiz notiz)
    {
        Notiz createdNotice = notizenService.saveNotices(notiz);

        LOGGER.info("Notiz erstellen mit Titel: " + notiz.getTitle() + " und Id: " + notiz.getId() + " und Zeitstempel: " + notiz.getCreateDate());

        return createdNotice;
    }

    // Vorhandene Notiz verändern
    @PUT
    @Path("/notiz")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateNotice(@Valid Notiz notiz)
    {
        Boolean success = notizenService.updateNotices(notiz);
        Response.Status responseStatus = success ? Response.Status.OK : Response.Status.CONFLICT;
        LOGGER.info("Notiz updaten mit Titel: " + notiz.getTitle() + " und Id: " + notiz.getId() + " und Zeitstempel: " + notiz.getCreateDate());

        return Response.status(responseStatus).build();
    }

    // Vorhandene Notiz löschen
    @DELETE
    @Path("/notiz")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response removeNotice(@Valid Notiz notiz)
    {
        Boolean success = notizenService.removeNotices(notiz);
        Response.Status responseStatus = success ? Response.Status.OK : Response.Status.CONFLICT;
        LOGGER.info("Notiz löschen mit Titel: " + notiz.getTitle() + " und Id: " + notiz.getId() + " und Zeitstempel: " + notiz.getCreateDate());

        return Response.status(responseStatus).build();
    }



    // Alle Notizen ermitteln
    @GET
    @Path("/notiz/search/all")
    public List<Notiz>getAllNotices()
    {
        return notizenService.getAllNotices();
    }

    // Notiz über ID ermitteln
    @GET
    @Path("/notiz/search/id")
    public Notiz getAllNoticesById(@QueryParam("id") Long id)
    {
        return notizenService.findById(id);
    }

    // Notiz über ihren Titel ermitteln
    @GET
    @Path("/notiz/search/title")
    public List<Notiz> getAllNoticesByTitle(@QueryParam("title") String title)
    {
        return notizenService.getAllNoticesByTitle(title);
    }

    // Notiz über das Erstelldatum ermitteln
    @GET
    @Path("/notiz/search/create")
    public List<Notiz>getAllNoticesByCreateDate(@QueryParam("date") String date)
    {
        return notizenService.getAllNoticesByCreateDate(date);
    }

    // Notiz über ihr Updatedatum ermitteln
    @GET
    @Path("/notiz/search/update")
    public List<Notiz>getAllNoticesByUpdateDate(@QueryParam("date") String date)
    {
        return notizenService.getAllNoticesByUpdateDate(date);
    }


    // Volltextsuche
    // Notiz über ID, Titel, Farbe und Text ermitteln
    @GET
    @Path("/notiz/search/text")
    public List<Notiz> getAllNoticesByText(@QueryParam("text") String text){
        return notizenService.getAllNoticesByText(text);
    }

}
