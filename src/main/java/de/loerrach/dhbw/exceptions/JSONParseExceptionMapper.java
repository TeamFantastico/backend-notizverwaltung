package de.loerrach.dhbw.exceptions;

// Externe Verweise
import javax.json.stream.JsonParsingException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class JSONParseExceptionMapper implements ExceptionMapper<JsonParsingException> {

    // Overrides
    @Override
    public Response toResponse(JsonParsingException exception) {
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
