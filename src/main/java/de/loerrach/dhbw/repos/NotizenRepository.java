package de.loerrach.dhbw.repos;

// Interne Verweise
import de.loerrach.dhbw.entity.Notiz;

// Externe Verweise
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.logging.Logger;

public class NotizenRepository {

    // Console Logger
    private final Logger LOGGER = Logger.getLogger(NotizenRepository.class.getName());

    @PersistenceContext(unitName = "notiz")
    private EntityManager entityManager;


    // DB Transaktionen
    @Transactional
    public void persistNotice(Notiz notiz)
    {
        entityManager.persist(notiz);
    }

    @Transactional
    public void updateNotice(Notiz notiz)
    {
        entityManager.merge(notiz);
    }

    @Transactional
    public void removeNotice(Notiz notiz)
    {
        Notiz notizDeletable = entityManager.find(Notiz.class, notiz.getId());

        entityManager.remove(notizDeletable);
    }


    // Such Funktionen
    public Notiz findNoticeById(Long id)
    {
        return entityManager.find(Notiz.class, id);
    }

    public List<Notiz> findAll()
    {
        Query query = entityManager.createQuery("SELECT n from Notiz n order by n.Id");

        return query.getResultList();
    }

    public List<Notiz> findByCreateDate(String date)
    {
        Query query = entityManager.createQuery("select n from Notiz n where n.CreateDate = :date order by n.CreateDate, n.Id");

        query.setParameter("date", date);

        return query.getResultList();
    }

    public List<Notiz> findByUpdateDate(String date)
    {
        Query query = entityManager.createQuery("select n from Notiz n where n.UpdateDate >= :date order by n.UpdateDate, n.Id");

        query.setParameter("date", date);

        return query.getResultList();
    }

    public List<Notiz> findByTitle(String title)
    {
        Query query = entityManager.createQuery("select n from Notiz n where n.Title = :title order by n.Id");
        query.setParameter("title", title);

        return query.getResultList();
    }

}
