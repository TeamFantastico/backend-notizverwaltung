package de.loerrach.dhbw.service;

// Interne Verweise
import de.loerrach.dhbw.entity.Notiz;
import de.loerrach.dhbw.repos.NotizenRepository;

// Externe Verweise
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class NotizenService {

    @Inject
    private NotizenRepository notizenRepository;

    // Console Logger
    private final Logger LOGGER = Logger.getLogger(NotizenService.class.getName());

    public Notiz saveNotices(Notiz notiz){
        try
        {
            notizenRepository.persistNotice(notiz);

            Notiz createdNotice = getNote(notiz);
            return createdNotice;
        }
        catch (Exception e)
        {
            LOGGER.severe(e.getMessage());
            return null;
        }
    }

    public Notiz getNote(Notiz notiz){
        return notiz;
    }

    public Boolean updateNotices(Notiz notiz){
        try
        {
            notizenRepository.updateNotice(notiz);
            return true;
        }
        catch (Exception e)
        {
            LOGGER.severe(e.getMessage());
            return false;
        }
    }

    public Boolean removeNotices(Notiz notiz){
        try
        {
            notizenRepository.removeNotice(notiz);
            return true;
        }
        catch (Exception e)
        {
            LOGGER.severe(e.getMessage());
            return false;
        }
    }

    public Notiz findById(Long id){
        try
        {
            return notizenRepository.findNoticeById(id);
        }
        catch (Exception e)
        {
            LOGGER.severe(e.getMessage());
            return null;
        }
    }

    public List<Notiz> getAllNoticesByCreateDate(String date)
    {
        try
        {
            return notizenRepository.findByCreateDate(date);
        }
        catch (Exception e)
        {
            LOGGER.severe(e.getMessage());
            return null;
        }
    }

    public List<Notiz> getAllNoticesByUpdateDate(String date)
    {
        try
        {
            return notizenRepository.findByUpdateDate(date);
        }
        catch (Exception e)
        {
            LOGGER.severe(e.getMessage());
            return null;
        }
    }

    public List<Notiz> getAllNoticesByTitle(String title)
    {
        try
        {
            return notizenRepository.findByTitle(title);
        }
        catch (Exception e)
        {
            LOGGER.severe(e.getMessage());
            return null;
        }
    }

    public List<Notiz> getAllNotices(){
        try
        {
            return notizenRepository.findAll();
        }
        catch (Exception e)
        {
            LOGGER.severe(e.getMessage());
            return null;
        }
    }

    public List<Notiz> getAllNoticesByText(String text){

        try
        {
            if(text == null) return null;

            List<Notiz> list = notizenRepository.findAll();
            List<Notiz> resuList = new ArrayList<Notiz>();

            for ( Notiz n :list)
            {
                if (n == null) continue;

                String searchString = n.getSearchString();

                if(searchString == null) continue;

                searchString = searchString.toUpperCase();
                text = text.toUpperCase();

                boolean isFound = searchString.contains(text);

                if (isFound){
                    resuList.add(n);
                }

                String[] splited = text.split("\\s+");
                for (String s :splited) {
                   boolean isFoundAtSecondTry = searchString.contains(s);

                   if (isFoundAtSecondTry){
                       if (resuList.contains(n)) continue;
                       resuList.add(n);
                   }
                }
            }
            return resuList;
        }
        catch (Exception e)
        {
            LOGGER.severe(e.getMessage());
            return null;
        }
    }
}
