package de.loerrach.dhbw.entity;

// Externe Verweise
import org.apache.commons.lang3.StringUtils;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

@Entity
public class Notiz {

    // Properties
    @Id
    @GeneratedValue
    private Long Id;

    @NotNull
    @Size(min = 3, max = 30)
    private String Title;

    private String Color;

    private String CreateDate;

    private String UpdateDate;

    @NotNull
    @Size(min = 5, max = 500)
    private String Text;

    private String SearchString;

    // Konstruktor
    protected Notiz(){}

    public Notiz(String title, String color, String text)
    {
        this.Title = title;
        this.Color = color;
        this.Text = text;
    }


    // Method die beim erstellen ausgelöst wird
    @PrePersist
    protected void onCreate()
    {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        String date = simpleDateFormat.format(new Date());
        CreateDate = UpdateDate = date;

        final String fullSearchString = StringUtils.join(Arrays.asList(
                Title,
                Text
        ), " ");
        this.SearchString = StringUtils.substring(fullSearchString, 0, 999);
    }

    // Method die beim updaten ausgelöst wird
    @PreUpdate
    protected void onUpdate()
    {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        String date = simpleDateFormat.format(new Date());
        UpdateDate = date;

        final String fullSearchString;
        fullSearchString = StringUtils.join(Arrays.asList(
                Title,
                Text
        ), " ");
        this.SearchString = StringUtils.substring(fullSearchString, 0, 999);
    }


    // Getter und Setter
    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }

    public String getUpdateDate() {
        return UpdateDate;
    }

    public void setUpdateDate(String updateDate) {
        UpdateDate = updateDate;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public String getSearchString() {
        return SearchString;
    }

    public void setSearchString(String searchString) {
        SearchString = searchString;
    }


    // Overrides

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Notiz notiz = (Notiz) o;
        return Objects.equals(Id, notiz.Id) &&
                Objects.equals(Title, notiz.Title) &&
                Objects.equals(Color, notiz.Color) &&
                Objects.equals(CreateDate, notiz.CreateDate) &&
                Objects.equals(UpdateDate, notiz.UpdateDate) &&
                Objects.equals(Text, notiz.Text) &&
                Objects.equals(SearchString, notiz.SearchString);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id, Title, Color, CreateDate, UpdateDate, Text, SearchString);
    }
}
