FROM tomee:8-jre-8.0.0-M2-webprofile

COPY /target/notizen-1.0-SNAPSHOT.war /usr/local/tomee/webapps/notizen.war

EXPOSE 8080